CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module alloy you to fetch your calculoid calculator to your site. You can
add multiple calculators from different or same accounts and calculators will
be available as blocks so you can put them where you want on the site.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/calculoid_calculators

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/calculoid_calculators

REQUIREMENTS
------------

This module requires the following modules:

 * Block

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions:

   - Add calculoid calculator

     Give user access to add his calculoid calculator to the system.

   - Delete calculoid calculator

     Give user access to delete calculoid calculator from system.

 * Manage calculoid calculators in  Administration » Structure » Calculoid 
 Calculators menu.

MAINTAINERS
-----------

Current maintainers:
 * Sagar (sagarwani) - https://www.drupal.org/user/3221452