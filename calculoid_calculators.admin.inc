<?php

/**
* @file
* calculoid_calculators.admin.inc
*/

/**
 * Function to display list of available calculators on site
 */
function calculoid_calculators_list_calculators(){
  $all_calculators = calculoid_calculators_get_calculator();
  if (!empty($all_calculators)){
    foreach ($all_calculators as $calculator) {
    	$links = array(
    	  array('title' => t('View'), 'href' => 'admin/structure/calculoid/' . $calculator->id),
        array('title' => t('Edit'), 'href' => 'admin/structure/calculoid/' . $calculator->id . '/edit'),
        array('title' => t('Delete'), 'href' => 'admin/structure/calculoid/' . $calculator->id . '/delete'),
      );
      $rows[] = array(
        'calculator_id' => $calculator->calculator_id,
        'calculoid_api_key' => str_repeat('*', strlen($calculator->calculoid_api_key) - 4) . substr($calculator->calculoid_api_key, -4),
        'calculoid_block_id' => $calculator->calculoid_block_id,
        theme('links__ctools_dropbutton', array('links' => $links)),
      );
    }
    $headers = array(
      t('Id'),
      t('API Key'),
      t('Block Id'),
      t('Oprations'),
    );
    $output = theme('table', array('header' => $headers, 'rows' => $rows));
  }
  else{
    $output = "<h2 style='text-align:center;'>" . t('No Calculator Available.') . "</h2>";
  }
  return $output;
}

function calculoid_calculators_display_calculator($calculator_id = NULL){
  $calculator = calculoid_calculators_get_calculator($calculator_id);
  return array(
    '#theme' => 'calculoid_calculator_display',
    '#calculator' => $calculator,
  );
}

/**
 * add calculoid calculator form
 */
function calculoid_calculators_add_calculator_form($form, $form_state){
  $form['calculator_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Calculator Id'),
    '#description' => t('id of the calculator provided by calculoid.'),
    '#required' => TRUE,
  );
  $form['calculoid_api'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#description' => t('api key provided by calculoid.'),
    '#required' => TRUE,
  );
  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  $form['actions']['cancel'] = array(
    '#type' => 'markup',
    '#markup' => l(t('Cancel'), 'admin/structure/calculoid'),
  );
  return $form;
}

function calculoid_calculators_add_calculator_form_validate($form, &$form_state){
  $values = $form_state['values'];
  $query = db_select('calculoid_calculators_data', 'ccd');
  $query->fields('ccd');
  $query->condition('ccd.calculator_id', $values['calculator_id']);
  $result = $query->execute()->fetchObject();
  if ($result) {
    form_set_error('calculator_id', t('A calculator with same id exist'));
  }
}

function calculoid_calculators_add_calculator_form_submit($form, &$form_state){
  $values = $form_state['values'];
  $calculator = array(
    'calculator_id' => $values['calculator_id'],
    'calculoid_api' => $values['calculoid_api'],
  );
  calculoid_calculators_save_calculator($calculator);
  drupal_set_message(t('Calculoid calculator "@calculator_id" has been saved.', array('@calculator_id' => $values['calculator_id'])));
  $form_state['redirect'] = 'admin/structure/calculoid';
}

/**
 * edit calculoid calculator form
 */
function calculoid_calculators_edit_calculator_form($form, $form_state, $calculator_id){
	$calculator = calculoid_calculators_get_calculator($calculator_id);
  if (is_object($calculator)) {
    $form['calculator_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Calculator Id'),
      '#description' => t('id of the calculator provided by calculoid.'),
      '#required' => TRUE,
      '#default_value' => $calculator->calculator_id,
    );
    $form['calculoid_api'] = array(
      '#type' => 'textfield',
      '#title' => t('API Key'),
      '#description' => t('api key provided by calculoid.'),
      '#required' => TRUE,
      '#default_value' => $calculator->calculoid_api_key,
    );
    $form['id'] = array(
      '#type' => 'hidden',
      '#value' => $calculator->id,
    );
    $form['actions'] = array(
      '#type' => 'actions',
    );
    $form['actions']['save'] = array(
      '#type' => 'submit',
      '#value' => t('Save')
    );
    $form['actions']['cancel'] = array(
      '#type' => 'markup',
      '#markup' => l(t('Cancel'), 'admin/structure/calculoid'),
    );
  }
  else {
    $form['error_message'] = array(
      '#type' => 'markup',
      '#markup' => t('There is an error with this request. Please contact to administrator.'),
    );
    $form['cancel'] = array(
      '#type' => 'markup',
      '#markup' => l(t('Cancel'), 'admin/structure/calculoid'),
    );
  }  
  return $form;
}

function calculoid_calculators_edit_calculator_form_submit($form, &$form_state){
  $values = $form_state['values'];
  $calculator = array(
    'id' => $values['id'],
    'calculator_id' => $values['calculator_id'],
    'calculoid_api' => $values['calculoid_api'],
  );
  calculoid_calculators_save_calculator($calculator);
  drupal_set_message(t('Calculoid calculator "@calculator_id" is updated.', array('@calculator_id' => $values['calculator_id'])));
  $form_state['redirect'] = 'admin/structure/calculoid';
}

/**
 * delete calculoid calculator form
 */
function calculoid_calculators_delete_calculator_form($form, $form_state, $calculator_id){
  $calculator = calculoid_calculators_get_calculator($calculator_id);
  if (is_object($calculator)) {
    $form['warning_message'] = array(
      '#type' => 'markup',
      '#markup' => '<h3>' . t('Do you want to delete "@calculator_id" calculator ? This action can not be undo.', array('@calculator_id' => $calculator->calculator_id)) . '</h3>'
    );
    $form['id'] = array(
      '#type' => 'hidden',
      '#value' => $calculator_id,
    );
    $form['actions'] = array(
      '#type' => 'actions',
    );
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete')
    );
    $form['actions']['cancel'] = array(
      '#type' => 'markup',
      '#markup' => l(t('Cancel'), 'admin/structure/calculoid'),
    );
  }
  else {
    $form['error_message'] = array(
      '#type' => 'markup',
      '#markup' => t('There is an error with this request. Please contact to administrator.'),
    );
    $form['cancel'] = array(
      '#type' => 'markup',
      '#markup' => l(t('Cancel'), 'admin/structure/calculoid'),
    );
  }  
  return $form;
}

function calculoid_calculators_delete_calculator_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $calculator = calculoid_calculators_get_calculator($values['id']);
  calculoid_calculators_delete_calculator($calculator->id);
  drupal_set_message(t('Calculator "@calculator_id" is deleted', array('@calculator_id' => $calculator->calculator_id)));
  $form_state['redirect'] = 'admin/structure/calculoid';
}